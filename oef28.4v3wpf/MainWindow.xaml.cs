﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace oef28._4v3wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Provincie> lijstProvincies = new List<Provincie>();
        public MainWindow()
        {
            InitializeComponent();
        }
        private void btnVoegProvincieToe_Click(object sender, RoutedEventArgs e)
        {
            cbSelecteerProvincie.Items.Clear();
            if(!string.IsNullOrEmpty(txtProvincieToevoegen.Text))
            {
                bool provincieBestaat = false;
                foreach(var item in lijstProvincies)
                {
                    if(item.Provincienaam.Equals(txtProvincieToevoegen.Text))
                    {
                        provincieBestaat = true;
                    }
                }
                if (!provincieBestaat)
                {
                    lijstProvincies.Add(new Provincie(
                        txtProvincieToevoegen.Text
                        ));
                } else {
                    MessageBox.Show("Deze provincie zit al in onze database.","Warning",MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                for (int i = 0; i < lijstProvincies.Count; i++)
                {
                    cbSelecteerProvincie.Items.Add(lijstProvincies[i].Provincienaam);
                }
                txtProvincieToevoegen.Text = string.Empty;
            } else
            {
                MessageBox.Show("Geen gemeentenaam ingegeven.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void btnAddGemeente_Click(object sender, RoutedEventArgs e)
        {
            lsGegevens.Items.Clear();
            lblProvincie.Content = string.Empty;
            if(cbSelecteerProvincie.SelectedIndex != -1)
            {
                if(!string.IsNullOrEmpty(txtGemeentenaam.Text) && !string.IsNullOrEmpty(txtPostcode.Text))
                {
                        if(cbSelecteerProvincie.SelectedItem.Equals(lijstProvincies[cbSelecteerProvincie.SelectedIndex].Provincienaam))
                        {
                            lijstProvincies[cbSelecteerProvincie.SelectedIndex].AddGemeente(new Gemeente(txtPostcode.Text, txtGemeentenaam.Text));
                            lsGegevens.Items.Add(lijstProvincies[cbSelecteerProvincie.SelectedIndex].ToString());
                            lblProvincie.Content = lijstProvincies[cbSelecteerProvincie.SelectedIndex].Provincienaam;
                        }
                    txtGemeentenaam.Text = string.Empty;
                    txtPostcode.Text = string.Empty;
                } else
                {
                    MessageBox.Show("Een of meerdere tekstvelden binnen Gemeente zijn niet ingevuld.");
                }
            } else
            {
                MessageBox.Show("Geen provincie geselecteerd.", "warning",MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void cbSelecteerProvincie_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lsGegevens.Items.Clear();
            lblProvincie.Content = string.Empty;
            if (cbSelecteerProvincie.SelectedIndex != -1)
            {
                if (cbSelecteerProvincie.SelectedItem.Equals(lijstProvincies[cbSelecteerProvincie.SelectedIndex].Provincienaam))
                {
                    lsGegevens.Items.Add(lijstProvincies[cbSelecteerProvincie.SelectedIndex].ToString());
                    lblProvincie.Content = lijstProvincies[cbSelecteerProvincie.SelectedIndex].Provincienaam;
                }
            }
        }
        private void btnVerwijderGemeente_Click(object sender, RoutedEventArgs e)
        {
            lsGegevens.Items.Clear();
            lblProvincie.Content = string.Empty;
            if (cbSelecteerProvincie.SelectedIndex != -1)
            {
                if (!string.IsNullOrEmpty(txtGemeentenaam.Text) && !string.IsNullOrEmpty(txtPostcode.Text))
                {
                    if (cbSelecteerProvincie.SelectedItem.Equals(lijstProvincies[cbSelecteerProvincie.SelectedIndex].Provincienaam))
                    {
                        lijstProvincies[cbSelecteerProvincie.SelectedIndex].RemoveGemeente(new Gemeente(txtPostcode.Text, txtGemeentenaam.Text));
                        lsGegevens.Items.Add(lijstProvincies[cbSelecteerProvincie.SelectedIndex].ToString());
                        lblProvincie.Content = lijstProvincies[cbSelecteerProvincie.SelectedIndex].Provincienaam;
                    }
                    txtGemeentenaam.Text = string.Empty;
                    txtPostcode.Text = string.Empty;
                }
                else
                {
                    MessageBox.Show("Een of meerdere tekstvelden binnen Gemeente zijn niet ingevuld.");
                }
            }
            else
            {
                MessageBox.Show("Geen provincie geselecteerd.", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
    class Gemeente
    {
        private string _postcode;
        private string _gemeenteNaam;

        public Gemeente() { }
        public Gemeente(string postcode, string gemeenteNaam)
        {
            _postcode = postcode;
            _gemeenteNaam = gemeenteNaam;
        }

        public override bool Equals(object obj)
        {
            bool isSame = false;
            if((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                isSame = false;
            } else
            {
                Gemeente gem = (Gemeente)obj;
                isSame = gem.GemeenteNaam == GemeenteNaam;
            }
            return isSame;
        }
        public override string ToString() => $"{Postcode} - {GemeenteNaam}";
        public string Postcode { get => _postcode; set => _postcode = value; }
        public string GemeenteNaam { get => _gemeenteNaam; set => _gemeenteNaam = value; }
    }
    class Provincie : Gemeente
    {
        private List<Gemeente> _gemeentes = new List<Gemeente>();
        private string _provincienaam;

        public Provincie() { }
        public Provincie(string provincienaam)
        {
            _provincienaam = provincienaam;
        }
        public void AddGemeente(Gemeente gemeente)
        {
            _gemeentes.Add(gemeente);
        }
        public void RemoveGemeente(Gemeente gemeente)
        {
            _gemeentes.Remove(gemeente);
        }
        public override string ToString()
        {
            string res = "";
            foreach(var item in Gemeentes)
            {
                res += item.ToString() + Environment.NewLine;
            }
            return res;
        }
        public string Provincienaam { get => _provincienaam; set => _provincienaam = value; }
        private List<Gemeente> Gemeentes { get => _gemeentes; set => _gemeentes = value; }
    }
}
